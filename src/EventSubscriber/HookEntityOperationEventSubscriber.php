<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\Entity\EntityOperationEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Entity Operation Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityOperationPluginManager $pluginManager
 */
class HookEntityOperationEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::ENTITY_OPERATION => 'onEntityOperation',
    ];
  }

  /**
   * On Entity Operation Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Entity\EntityOperationEvent $event
   *   The Entity Operation Event.
   */
  public function onEntityOperation(EntityOperationEvent $event) : void {
    $this->handleHooks($event);
  }

}
