<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\Views\ViewsPreViewEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Views Pre View Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\Views\HookViewsPreViewPluginManager $pluginManager
 */
class HookViewsPreViewEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::VIEWS_PRE_VIEW => 'onViewsPreView',
    ];
  }

  /**
   * On Views Pre View Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Views\ViewsPreViewEvent $event
   *   The Views Pre View Event.
   */
  public function onViewsPreView(ViewsPreViewEvent $event) : void {
    $this->handleHooks($event);
  }

}
