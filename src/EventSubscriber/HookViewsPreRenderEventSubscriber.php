<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\Views\ViewsPreRenderEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Views Pre Render Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\Views\HookViewsPreRenderPluginManager $pluginManager
 */
class HookViewsPreRenderEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::VIEWS_PRE_RENDER => 'onViewsPreRender',
    ];
  }

  /**
   * On Views Pre Render Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Views\ViewsPreRenderEvent $event
   *   The Views Pre Render Event.
   */
  public function onViewsPreRender(ViewsPreRenderEvent $event) : void {
    $this->handleHooks($event);
  }

}
