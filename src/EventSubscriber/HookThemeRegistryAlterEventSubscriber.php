<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\Theme\ThemeRegistryAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Theme Registry Alter Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\Theme\HookThemeRegistryAlterPluginManager $pluginManager
 */
class HookThemeRegistryAlterEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::THEME_REGISTRY_ALTER => 'onThemeRegistryAlter',
    ];
  }

  /**
   * On Theme Registry Alter Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Theme\ThemeRegistryAlterEvent $event
   *   The Theme Registry Alter Event.
   */
  public function onThemeRegistryAlter(ThemeRegistryAlterEvent $event) : void {
    $this->handleHooks($event);
  }

}
