<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\User\UserCancelEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the User Cancel Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\User\HookUserCancelPluginManager $pluginManager
 */
class HookUserCancelEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::USER_CANCEL => 'onUserCancel',
    ];
  }

  /**
   * On User Cancel Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\User\UserCancelEvent $event
   *   The User Cancel Event.
   */
  public function onUserCancel(UserCancelEvent $event) : void {
    $this->handleHooks($event);
  }

}
