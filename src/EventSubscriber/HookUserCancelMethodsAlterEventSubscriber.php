<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\User\UserCancelMethodsAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the User Cancel Methods Alter Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\User\HookUserCancelMethodsAlterPluginManager $pluginManager
 */
class HookUserCancelMethodsAlterEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::USER_CANCEL_METHODS_ALTER => 'onUserCancelMethodsAlter',
    ];
  }

  /**
   * On User Cancel Methods Alter Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\User\UserCancelMethodsAlterEvent $event
   *   The User Cancel Methods Alter Event.
   */
  public function onUserCancelMethodsAlter(UserCancelMethodsAlterEvent $event) : void {
    $this->handleHooks($event);
  }

}
