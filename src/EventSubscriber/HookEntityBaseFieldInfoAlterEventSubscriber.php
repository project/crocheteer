<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\EntityType\EntityBaseFieldInfoAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Entity Base Field Info Alter Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\EntityType\HookEntityBaseFieldInfoAlterPluginManager $pluginManager
 */
class HookEntityBaseFieldInfoAlterEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::ENTITY_BASE_FIELD_INFO_ALTER => 'onEntityBaseFieldInfoAlter',
    ];
  }

  /**
   * On Entity Base Field Info Alter Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\EntityType\EntityBaseFieldInfoAlterEvent $event
   *   The Entity Base Field Info Alter Event.
   */
  public function onEntityBaseFieldInfoAlter(EntityBaseFieldInfoAlterEvent $event) : void {
    $this->handleHooks($event);
  }

}
