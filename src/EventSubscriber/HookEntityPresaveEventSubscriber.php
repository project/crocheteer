<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\Entity\EntityPresaveEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Entity Presave Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityPresavePluginManager $pluginManager
 */
class HookEntityPresaveEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::ENTITY_PRE_SAVE => 'onEntityPresave',
    ];
  }

  /**
   * On Entity Presave Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Entity\EntityPresaveEvent $event
   *   The Entity Presave Event.
   */
  public function onEntityPresave(EntityPresaveEvent $event) : void {
    $this->handleHooks($event);
  }

}
