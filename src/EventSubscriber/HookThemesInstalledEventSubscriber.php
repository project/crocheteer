<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\Theme\ThemesInstalledEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Themes Installed Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\Theme\HookThemesInstalledPluginManager $pluginManager
 */
class HookThemesInstalledEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::THEMES_INSTALLED => 'onThemesInstalled',
    ];
  }

  /**
   * On Themes Installed Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Theme\ThemesInstalledEvent $event
   *   The Themes Installed Event.
   */
  public function onThemesInstalled(ThemesInstalledEvent $event) : void {
    $this->handleHooks($event);
  }

}
