<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\Views\ViewsPostRenderEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Views Post Render Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\Views\HookViewsPostRenderPluginManager $pluginManager
 */
class HookViewsPostRenderEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::VIEWS_POST_RENDER => 'onViewsPostRender',
    ];
  }

  /**
   * On Views Post Render Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Views\ViewsPostRenderEvent $event
   *   The Views Post Render Event.
   */
  public function onViewsPostRender(ViewsPostRenderEvent $event) : void {
    $this->handleHooks($event);
  }

}
