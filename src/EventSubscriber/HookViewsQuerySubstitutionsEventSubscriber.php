<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\Views\ViewsQuerySubstitutionsEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Views Query Substitutions Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\Views\HookViewsQuerySubstitutionsPluginManager $pluginManager
 */
class HookViewsQuerySubstitutionsEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::VIEWS_QUERY_SUBSTITUTIONS => 'onViewsQuerySubstitutions',
    ];
  }

  /**
   * On Views Query Substitutions Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Views\ViewsQuerySubstitutionsEvent $event
   *   The Views Query Substitutions Event.
   */
  public function onViewsQuerySubstitutions(ViewsQuerySubstitutionsEvent $event) : void {
    $this->handleHooks($event);
  }

}
