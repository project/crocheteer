<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\Entity\EntityViewAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Entity View Alter Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityViewAlterPluginManager $pluginManager
 */
class HookEntityViewAlterEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::ENTITY_VIEW_ALTER => 'onEntityViewAlter',
    ];
  }

  /**
   * On Entity View Alter Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Entity\EntityViewAlterEvent $event
   *   The Entity View Alter Event.
   */
  public function onEntityViewAlter(EntityViewAlterEvent $event) : void {
    $this->handleHooks($event);
  }

}
