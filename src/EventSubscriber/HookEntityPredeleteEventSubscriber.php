<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\Entity\EntityPredeleteEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Entity Predelete Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityPredeletePluginManager $pluginManager
 */
class HookEntityPredeleteEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::ENTITY_PRE_DELETE => 'onEntityPredelete',
    ];
  }

  /**
   * On Entity Predelete Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Entity\EntityPredeleteEvent $event
   *   The Entity Predelete Event.
   */
  public function onEntityPredelete(EntityPredeleteEvent $event) : void {
    $this->handleHooks($event);
  }

}
