<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\Theme\TemplatePreprocessDefaultVariablesAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for Template Preprocess Default Variables Alter Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\Theme\HookTemplatePreprocessDefaultVariablesAlterPluginManager $pluginManager
 */
class HookTemplatePreprocessDefaultVariablesAlterEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::TEMPLATE_PREPROCESS_DEFAULT_VARIABLES_ALTER => 'onTemplatePreprocessDefaultVariablesAlter',
    ];
  }

  /**
   * On Template Preprocess Default Variables Alter Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Theme\TemplatePreprocessDefaultVariablesAlterEvent $event
   *   The Template Preprocess Default Variables Alter Event.
   */
  public function onTemplatePreprocessDefaultVariablesAlter(TemplatePreprocessDefaultVariablesAlterEvent $event) : void {
    $this->handleHooks($event);
  }

}
