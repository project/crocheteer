<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\EntityExtra\EntityExtraFieldInfoAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Entity Extra Field Info Alter Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\EntityExtra\HookEntityExtraFieldInfoAlterPluginManager $pluginManager
 */
class HookEntityExtraFieldInfoAlterEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::ENTITY_EXTRA_FIELD_INFO_ALTER => 'onEntityExtraFieldInfoAlter',
    ];
  }

  /**
   * On Entity Extra Field Info Alter Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\EntityExtra\EntityExtraFieldInfoAlterEvent $event
   *   The Entity Extra Field Info Alter Event.
   */
  public function onEntityExtraFieldInfoAlter(EntityExtraFieldInfoAlterEvent $event) : void {
    $this->handleHooks($event);
  }

}
