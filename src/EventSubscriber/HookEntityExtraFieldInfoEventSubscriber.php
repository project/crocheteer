<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\EntityExtra\EntityExtraFieldInfoEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Entity Extra Field Info Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\EntityExtra\HookEntityExtraFieldInfoPluginManager $pluginManager
 */
class HookEntityExtraFieldInfoEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::ENTITY_EXTRA_FIELD_INFO => 'onEntityExtraFieldInfo',
    ];
  }

  /**
   * On Entity Extra Field Info Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\EntityExtra\EntityExtraFieldInfoEvent $event
   *   The Entity Extra Field Info Event.
   */
  public function onEntityExtraFieldInfo(EntityExtraFieldInfoEvent $event) : void {
    $this->handleHooks($event);
  }

}
