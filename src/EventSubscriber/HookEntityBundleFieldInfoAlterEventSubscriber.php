<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\EntityType\EntityBundleFieldInfoAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Entity Bundle Field Info Alter Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\EntityType\HookEntityBundleFieldInfoAlterPluginManager $pluginManager
 */
class HookEntityBundleFieldInfoAlterEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::ENTITY_BUNDLE_FIELD_INFO_ALTER => 'onEntityBundleFieldInfoAlter',
    ];
  }

  /**
   * On Entity Bundle Field Info Alter Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\EntityType\EntityBundleFieldInfoAlterEvent $event
   *   The Entity Bundle Field Info Alter Event.
   */
  public function onEntityBundleFieldInfoAlter(EntityBundleFieldInfoAlterEvent $event) : void {
    $this->handleHooks($event);
  }

}
