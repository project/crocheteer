<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\Entity\EntityBuildDefaultsAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Entity Build Defaults Alter Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityBuildDefaultsAlterPluginManager $pluginManager
 */
class HookEntityBuildDefaultsAlterEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::ENTITY_BUILD_DEFAULTS_ALTER => 'onEntityBuildDefaultsAlter',
    ];
  }

  /**
   * On Entity Build Defaults Alter Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Entity\EntityBuildDefaultsAlterEvent $event
   *   The Entity Build Defaults Alter Event.
   */
  public function onEntityBuildDefaultsAlter(EntityBuildDefaultsAlterEvent $event) : void {
    $this->handleHooks($event);
  }

}
