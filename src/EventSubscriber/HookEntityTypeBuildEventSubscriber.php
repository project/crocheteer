<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\EntityType\EntityTypeBuildEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Entity Type Build Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\EntityType\HookEntityTypeBuildPluginManager $pluginManager
 */
class HookEntityTypeBuildEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::ENTITY_TYPE_BUILD => 'onEntityTypeBuild',
    ];
  }

  /**
   * On Entity Type Build Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\EntityType\EntityTypeBuildEvent $event
   *   The Entity Type Build Event.
   */
  public function onEntityTypeBuild(EntityTypeBuildEvent $event) : void {
    $this->handleHooks($event);
  }

}
