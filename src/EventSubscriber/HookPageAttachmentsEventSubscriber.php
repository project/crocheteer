<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\Page\PageAttachmentsEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Page Attachments Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\Page\HookPageAttachmentsPluginManager $pluginManager
 */
class HookPageAttachmentsEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::PAGE_ATTACHMENTS => 'onPageAttachments',
    ];
  }

  /**
   * On Page Attachments Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Page\PageAttachmentsEvent $event
   *   The Page Attachments Event.
   */
  public function onPageAttachments(PageAttachmentsEvent $event) : void {
    $this->handleHooks($event);
  }

}
