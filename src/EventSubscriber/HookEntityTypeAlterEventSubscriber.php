<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\EntityType\EntityTypeAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Entity Type Alter Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\EntityType\HookEntityTypeAlterPluginManager $pluginManager
 */
class HookEntityTypeAlterEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::ENTITY_TYPE_ALTER => 'onEntityTypeAlter',
    ];
  }

  /**
   * On Entity Type Alter Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\EntityType\EntityTypeAlterEvent $event
   *   The Entity Type Alter Event.
   */
  public function onEntityTypeAlter(EntityTypeAlterEvent $event) : void {
    $this->handleHooks($event);
  }

}
