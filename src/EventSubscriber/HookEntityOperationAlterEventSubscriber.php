<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\Entity\EntityOperationAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Entity Operation Alter Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityOperationAlterPluginManager $pluginManager
 */
class HookEntityOperationAlterEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::ENTITY_OPERATION_ALTER => 'onEntityOperationAlter',
    ];
  }

  /**
   * On Entity Operation Alter Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Entity\EntityOperationAlterEvent $event
   *   The Entity Operation Alter Event.
   */
  public function onEntityOperationAlter(EntityOperationAlterEvent $event) : void {
    $this->handleHooks($event);
  }

}
