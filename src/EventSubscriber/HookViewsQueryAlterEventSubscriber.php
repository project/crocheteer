<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\Views\ViewsQueryAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Views Query Alter Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\Views\HookViewsQueryAlterPluginManager $pluginManager
 */
class HookViewsQueryAlterEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::VIEWS_QUERY_ALTER => 'onViewsQueryAlter',
    ];
  }

  /**
   * On Views Query Alter Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Views\ViewsQueryAlterEvent $event
   *   The Views Query Alter Event.
   */
  public function onViewsQueryAlter(ViewsQueryAlterEvent $event) : void {
    $this->handleHooks($event);
  }

}
