<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\Theme\LibraryInfoAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Library Info Alter Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\Theme\HookLibraryInfoAlterPluginManager $pluginManager
 */
class HookLibraryInfoAlterEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::LIBRARY_INFO_ALTER => 'onLibraryInfoAlter',
    ];
  }

  /**
   * On Library Info Alter Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Theme\LibraryInfoAlterEvent $event
   *   The Library Info Alter Event.
   */
  public function onLibraryInfoAlter(LibraryInfoAlterEvent $event) : void {
    $this->handleHooks($event);
  }

}
