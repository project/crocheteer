<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\Language\LanguageSwitchLinksAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Language Switch Links Alter Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\Language\HookLanguageSwitchLinksAlterPluginManager $pluginManager
 */
class HookLanguageSwitchLinksAlterEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::LANGUAGE_SWITCH_LINKS_ALTER => 'onLanguageSwitchLinksAlter',
    ];
  }

  /**
   * On Language Switch Links Alter Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Language\LanguageSwitchLinksAlterEvent $event
   *   The Language Switch Links Alter Event.
   */
  public function onLanguageSwitchLinksAlter(LanguageSwitchLinksAlterEvent $event) : void {
    $this->handleHooks($event);
  }

}
