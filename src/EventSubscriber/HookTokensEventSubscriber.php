<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\Token\TokensReplacementEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Register Hooks handler for the Tokens Replacement Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\Token\HookTokensPluginManager $pluginManager
 */
class HookTokensEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::TOKEN_REPLACEMENT => 'onTokensReplacement',
    ];
  }

  /**
   * On Tokens Replacement Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Token\TokensReplacementEvent $event
   *   The Tokens Replacement Event.
   */
  public function onTokensReplacement(TokensReplacementEvent $event) : void {
    $this->handleHooks($event);
  }

}
