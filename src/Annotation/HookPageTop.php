<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Page Top.
 *
 * @see \hook_page_top()
 * @see \Drupal\crocheteer\Plugin\Hook\Page\HookPageTopPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Page\HookPageTopPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookPageTopEventSubscriber
 *
 * @Annotation
 */
class HookPageTop extends Hook {}
