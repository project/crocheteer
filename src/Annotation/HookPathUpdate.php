<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Path Update.
 *
 * @see \hook_path_update()
 * @see \Drupal\crocheteer\Plugin\Hook\Path\HookPathUpdatePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Path\HookPathUpdatePluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookPathUpdateEventSubscriber
 *
 * @Annotation
 */
class HookPathUpdate extends Hook {}
