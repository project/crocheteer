<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Theme Suggestions Alter.
 *
 * @see \hook_theme_suggestions_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\Theme\HookThemeSuggestionsAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Theme\HookThemeSuggestionsAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookThemeSuggestionsAlterEventSubscriber
 *
 * @Annotation
 */
class HookThemeSuggestionsAlter extends Hook {}
