<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Field Widget Form Alter.
 *
 * The widgetTypes property can be used to narrow down objects affected by the
 * Widget Form Alter Hook, serving as an alternative to WIDGET_TYPE-based hook
 * declarations.
 *
 * @see \hook_field_widget_form_alter()
 * @see \hook_field_widget_WIDGET_TYPE_form_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\Form\HookFieldWidgetFormAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Form\HookFieldWidgetFormAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookFieldWidgetFormAlterEventSubscriber
 *
 * @Annotation
 */
class HookFieldWidgetFormAlter extends Hook {

  /**
   * Array of relevant Widget Types.
   *
   * @var string[]
   */
  public array $widgetTypes;

}
