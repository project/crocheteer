<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Access.
 *
 * @see \hook_entity_access()
 * @see \hook_ENTITY_TYPE_access()
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAccessPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAccessPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityAccessEventSubscriber
 *
 * @Annotation
 */
class HookEntityAccess extends HookEntity {}
