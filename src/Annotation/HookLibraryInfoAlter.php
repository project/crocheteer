<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Library Info Alter.
 *
 * @see \hook_library_info_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\Theme\HookLibraryInfoAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Theme\HookLibraryInfoAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookLibraryInfoAlterEventSubscriber
 *
 * @Annotation
 */
class HookLibraryInfoAlter extends Hook {}
