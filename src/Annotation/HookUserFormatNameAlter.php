<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook User Format Name Alter.
 *
 * @see \hook_user_format_name_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\User\HookUserFormatNameAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\User\HookUserFormatNameAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookUserFormatNameAlterEventSubscriber
 *
 * @Annotation
 */
class HookUserFormatNameAlter extends Hook {}
