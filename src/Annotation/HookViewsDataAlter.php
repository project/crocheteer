<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Views Data Alter.
 *
 * @see \hook_views_data_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsDataAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsDataAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookViewsDataAlterEventSubscriber
 *
 * @Annotation
 */
class HookViewsDataAlter extends Hook {}
