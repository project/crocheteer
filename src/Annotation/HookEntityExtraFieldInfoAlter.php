<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Extra Field Info Alter.
 *
 * @see \hook_entity_extra_field_info_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\EntityExtra\HookEntityExtraFieldInfoAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\EntityExtra\HookEntityExtraFieldInfoAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityExtraFieldInfoAlterEventSubscriber
 *
 * @Annotation
 */
class HookEntityExtraFieldInfoAlter extends Hook {}
