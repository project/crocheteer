<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Base class for all Hook Annotations.
 *
 * Contains properties global to all Hook Annotations. The id property must be
 * set to a unique identifier (machine name) for the Hook Annotation, while the
 * title property may simply be a readable name.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 */
abstract class Hook extends Plugin {

  /**
   * The Hook Plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The Hook Plugin title.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $title;

}
