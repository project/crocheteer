<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Bundle Field Info Alter.
 *
 * @see \hook_entity_bundle_field_info_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\EntityType\HookEntityBundleFieldInfoAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\EntityType\HookEntityBundleFieldInfoAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityBundleFieldInfoAlterEventSubscriber
 *
 * @Annotation
 */
class HookEntityBundleFieldInfoAlter extends Hook {}
