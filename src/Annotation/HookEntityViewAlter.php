<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity View Alter.
 *
 * @see \hook_entity_view_alter()
 * @see \hook_ENTITY_TYPE_view_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityViewAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityViewAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityViewAlterEventSubscriber
 *
 * @Annotation
 */
class HookEntityViewAlter extends HookEntity {}
