<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Views Post Build.
 *
 * @see \hook_views_post_build()
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsPostBuildPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsPostBuildPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookViewsPostBuildEventSubscriber
 *
 * @Annotation
 */
class HookViewsPostBuild extends Hook {}
