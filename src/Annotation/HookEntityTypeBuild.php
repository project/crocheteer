<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Type Build.
 *
 * @see \hook_entity_type_build()
 * @see \Drupal\crocheteer\Plugin\Hook\EntityType\HookEntityTypeBuildPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\EntityType\HookEntityTypeBuildPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityTypeBuildEventSubscriber
 *
 * @Annotation
 */
class HookEntityTypeBuild extends Hook {}
