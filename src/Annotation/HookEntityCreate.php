<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Create.
 *
 * @see \hook_entity_create()
 * @see \hook_ENTITY_TYPE_create()
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityCreatePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityCreatePluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityCreateEventSubscriber
 *
 * @Annotation
 */
class HookEntityCreate extends HookEntity {}
