<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Build Defaults Alter.
 *
 * @see \hook_entity_build_defaults_alter()
 * @see \hook_ENTITY_TYPE_build_defaults_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityBuildDefaultsAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityBuildDefaultsAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityBuildDefaultsAlterEventSubscriber
 *
 * @Annotation
 */
class HookEntityBuildDefaultsAlter extends HookEntity {}
