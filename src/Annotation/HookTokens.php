<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Tokens.
 *
 * @see \hook_tokens()
 * @see \Drupal\crocheteer\Plugin\Hook\Token\HookTokensPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Token\HookTokensPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookTokensEventSubscriber
 *
 * @Annotation
 */
class HookTokens extends Hook {}
