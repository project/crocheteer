<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Toolbar Alter.
 *
 * @see \hook_toolbar_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\Toolbar\HookToolbarAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Toolbar\HookToolbarAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookToolbarAlterEventSubscriber
 *
 * @Annotation
 */
class HookToolbarAlter extends Hook {}
