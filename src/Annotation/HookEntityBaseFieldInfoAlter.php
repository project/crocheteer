<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Base Field Info Alter.
 *
 * @see \hook_entity_base_field_info_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\EntityType\HookEntityBaseFieldInfoAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\EntityType\HookEntityBaseFieldInfoAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityBaseFieldInfoAlterEventSubscriber
 *
 * @Annotation
 */
class HookEntityBaseFieldInfoAlter extends Hook {}
