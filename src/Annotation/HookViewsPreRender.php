<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Views Pre Render.
 *
 * @see \hook_views_pre_render()
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsPreRenderPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsPreRenderPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookViewsPreRenderEventSubscriber
 *
 * @Annotation
 */
class HookViewsPreRender extends Hook {}
