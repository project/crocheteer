<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Extra Field Info.
 *
 * @see \hook_entity_extra_field_info()
 * @see \Drupal\crocheteer\Plugin\Hook\EntityExtra\HookEntityExtraFieldInfoPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\EntityExtra\HookEntityExtraFieldInfoPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityExtraFieldInfoEventSubscriber
 *
 * @Annotation
 */
class HookEntityExtraFieldInfo extends Hook {}
