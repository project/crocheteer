<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Predelete.
 *
 * @see \hook_entity_predelete()
 * @see \hook_ENTITY_TYPE_predelete()
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityPredeletePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityPredeletePluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityPredeleteEventSubscriber
 *
 * @Annotation
 */
class HookEntityPredelete extends HookEntity {}
