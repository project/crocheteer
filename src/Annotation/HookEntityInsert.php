<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Insert.
 *
 * @see \hook_entity_insert()
 * @see \hook_ENTITY_TYPE_insert()
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityInsertPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityInsertPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityInsertEventSubscriber
 *
 * @Annotation
 */
class HookEntityInsert extends HookEntity {}
