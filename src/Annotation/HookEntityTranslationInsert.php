<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Translation Insert.
 *
 * @see \hook_entity_translation_insert()
 * @see \hook_ENTITY_TYPE_translation_insert()
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityTranslationInsertPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityTranslationInsertPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityTranslationInsertEventSubscriber
 *
 * @Annotation
 */
class HookEntityTranslationInsert extends HookEntity {}
