<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Form Alter.
 *
 * @see \hook_form_alter()
 * @see \hook_form_FORM_ID_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\Form\HookFormAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Form\HookFormAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookFormAlterEventSubscriber
 *
 * @Annotation
 */
class HookFormAlter extends HookForm {}
