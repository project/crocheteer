<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook User Cancel.
 *
 * @see \hook_user_cancel()
 * @see \Drupal\crocheteer\Plugin\Hook\User\HookUserCancelPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\User\HookUserCancelPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookUserCancelEventSubscriber
 *
 * @Annotation
 */
class HookUserCancel extends Hook {}
