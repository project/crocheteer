<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Delete.
 *
 * @see \hook_entity_delete()
 * @see \hook_ENTITY_TYPE_delete()
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityDeletePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityDeletePluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityDeleteEventSubscriber
 *
 * @Annotation
 */
class HookEntityDelete extends HookEntity {}
