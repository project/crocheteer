<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Views Pre Execute.
 *
 * @see \hook_views_pre_execute()
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsPreExecutePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsPreExecutePluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookViewsPreExecuteEventSubscriber
 *
 * @Annotation
 */
class HookViewsPreExecute extends Hook {}
