<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Template Preprocess Default Variables Alter.
 *
 * @see \hook_template_preprocess_default_variables_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\Theme\HookTemplatePreprocessDefaultVariablesAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Theme\HookTemplatePreprocessDefaultVariablesAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookTemplatePreprocessDefaultVariablesAlterEventSubscriber
 *
 * @Annotation
 */
class HookTemplatePreprocessDefaultVariablesAlter extends Hook {}
