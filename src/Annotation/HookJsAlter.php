<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Js Alter.
 *
 * @see \hook_js_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\Theme\HookJsAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Theme\HookJsAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookJsAlterEventSubscriber
 *
 * @Annotation
 */
class HookJsAlter extends Hook {}
