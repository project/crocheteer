<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Views Post Execute.
 *
 * @see \hook_views_post_execute()
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsPostExecutePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsPostExecutePluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookViewsPostExecuteEventSubscriber
 *
 * @Annotation
 */
class HookViewsPostExecute extends Hook {}
