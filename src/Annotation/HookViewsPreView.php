<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Views Pre View.
 *
 * @see \hook_views_pre_view()
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsPreViewPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsPreViewPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookViewsPreViewEventSubscriber
 *
 * @Annotation
 */
class HookViewsPreView extends Hook {}
