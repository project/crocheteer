<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Path Insert.
 *
 * @see \hook_path_insert()
 * @see \Drupal\crocheteer\Plugin\Hook\Path\HookPathInsertPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Path\HookPathInsertPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookPathInsertEventSubscriber
 *
 * @Annotation
 */
class HookPathInsert extends Hook {}
