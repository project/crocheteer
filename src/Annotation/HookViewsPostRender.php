<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Views Post Render.
 *
 * @see \hook_views_post_render()
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsPostRenderPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsPostRenderPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookViewsPostRenderEventSubscriber
 *
 * @Annotation
 */
class HookViewsPostRender extends Hook {}
