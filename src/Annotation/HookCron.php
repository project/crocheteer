<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Cron.
 *
 * @see \hook_cron()
 * @see \Drupal\crocheteer\Plugin\Hook\Cron\HookCronPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Cron\HookCronPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookCronEventSubscriber
 *
 * @Annotation
 */
class HookCron extends Hook {}
