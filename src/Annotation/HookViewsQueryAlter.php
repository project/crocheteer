<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Views Query Alter.
 *
 * @see \hook_views_query_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsQueryAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsQueryAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookViewsQueryAlterEventSubscriber
 *
 * @Annotation
 */
class HookViewsQueryAlter extends Hook {}
