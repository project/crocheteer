<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity View.
 *
 * @see \hook_entity_view()
 * @see \hook_ENTITY_TYPE_view()
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityViewPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityViewPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityViewEventSubscriber
 *
 * @Annotation
 */
class HookEntityView extends HookEntity {}
