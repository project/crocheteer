<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Theme Registry Alter.
 *
 * @see \hook_theme_registry_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\Theme\HookThemeRegistryAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Theme\HookThemeRegistryAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookThemeRegistryAlterEventSubscriber
 *
 * @Annotation
 */
class HookThemeRegistryAlter extends Hook {}
