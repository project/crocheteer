<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Field Access.
 *
 * @see \hook_entity_field_access()
 * @see \Drupal\crocheteer\Plugin\Hook\EntityField\HookEntityFieldAccessPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\EntityField\HookEntityFieldAccessPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityFieldAccessEventSubscriber
 *
 * @Annotation
 */
class HookEntityFieldAccess extends Hook {}
