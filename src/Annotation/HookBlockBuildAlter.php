<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Block Build Alter.
 *
 * @see \hook_block_build_alter()
 * @see \hook_block_build_BASE_BLOCK_ID_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\Block\HookBlockBuildAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Block\HookBlockBuildAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookBlockBuildAlterEventSubscriber
 *
 * @Annotation
 */
class HookBlockBuildAlter extends HookBlock {}
