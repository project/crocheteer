<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Translation Delete.
 *
 * @see \hook_entity_translation_delete()
 * @see \hook_ENTITY_TYPE_translation_delete()
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityTranslationDeletePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityTranslationDeletePluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityTranslationDeleteEventSubscriber
 *
 * @Annotation
 */
class HookEntityTranslationDelete extends HookEntity {}
