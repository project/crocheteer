<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Page Attachments.
 *
 * @see \hook_page_attachments()
 * @see \Drupal\crocheteer\Plugin\Hook\Page\HookPageAttachmentsPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Page\HookPageAttachmentsPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookPageAttachmentsEventSubscriber
 *
 * @Annotation
 */
class HookPageAttachments extends Hook {}
