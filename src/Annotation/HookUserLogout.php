<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook User Logout.
 *
 * @see \hook_user_logout()
 * @see \Drupal\crocheteer\Plugin\Hook\User\HookUserLogoutPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\User\HookUserLogoutPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookUserLogoutEventSubscriber
 *
 * @Annotation
 */
class HookUserLogout extends Hook {}
