<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Base class for all Hook Form Annotations.
 *
 * Contains properties global to all Hook Form Annotations. The formIds
 * property can be used to narrow down objects affected by the Form Hook,
 * serving as an alternative to FORM_ID-based hook declarations.
 */
abstract class HookForm extends Hook {

  /**
   * Array of relevant Form IDs.
   *
   * @var string[]
   */
  public array $formIds;

}
