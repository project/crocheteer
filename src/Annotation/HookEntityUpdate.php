<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Update.
 *
 * @see \hook_entity_update()
 * @see \hook_ENTITY_TYPE_update()
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityUpdatePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityUpdatePluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityUpdateEventSubscriber
 *
 * @Annotation
 */
class HookEntityUpdate extends HookEntity {}
