<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Load.
 *
 * The entityTypes property can be used to narrow down objects affected by the
 * Entity Load Hook, serving as an alternative to ENTITY_TYPE-based hook
 * declarations.
 *
 * @see \hook_entity_load()
 * @see \hook_ENTITY_TYPE_load()
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityLoadPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityLoadPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityLoadEventSubscriber
 *
 * @Annotation
 */
class HookEntityLoad extends Hook {

  /**
   * Array of relevant ENTITY_TYPEs.
   *
   * @var string[]
   */
  public array $entityTypes;

}
