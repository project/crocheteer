<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Type Alter.
 *
 * @see \hook_entity_type_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\EntityType\HookEntityTypeAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\EntityType\HookEntityTypeAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityTypeAlterEventSubscriber
 *
 * @Annotation
 */
class HookEntityTypeAlter extends Hook {}
