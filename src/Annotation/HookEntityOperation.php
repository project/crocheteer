<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Operation.
 *
 * @see \hook_entity_operation()
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityOperationPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityOperationPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityOperationEventSubscriber
 *
 * @Annotation
 */
class HookEntityOperation extends Hook {}
