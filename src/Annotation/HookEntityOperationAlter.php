<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Operation Alter.
 *
 * @see \hook_entity_operation_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityOperationAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityOperationAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityOperationAlterEventSubscriber
 *
 * @Annotation
 */
class HookEntityOperationAlter extends Hook {}
