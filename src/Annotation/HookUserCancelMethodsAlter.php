<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook User Cancel Methods Alter.
 *
 * @see \hook_user_cancel_methods_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\User\HookUserCancelMethodsAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\User\HookUserCancelMethodsAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookUserCancelMethodsAlterEventSubscriber
 *
 * @Annotation
 */
class HookUserCancelMethodsAlter extends Hook {}
