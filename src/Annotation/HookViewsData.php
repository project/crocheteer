<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Views Data.
 *
 * @see \hook_views_data()
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsDataPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsDataPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookViewsDataEventSubscriber
 *
 * @Annotation
 */
class HookViewsData extends Hook {}
