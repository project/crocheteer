<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Path Delete.
 *
 * @see \hook_path_delete()
 * @see \Drupal\crocheteer\Plugin\Hook\Path\HookPathDeletePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Path\HookPathDeletePluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookPathDeleteEventSubscriber
 *
 * @Annotation
 */
class HookPathDelete extends Hook {}
