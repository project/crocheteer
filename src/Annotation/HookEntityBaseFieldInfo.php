<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Base Field Info.
 *
 * @see \hook_entity_base_field_info()
 * @see \Drupal\crocheteer\Plugin\Hook\EntityType\HookEntityBaseFieldInfoPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\EntityType\HookEntityBaseFieldInfoPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityBaseFieldInfoEventSubscriber
 *
 * @Annotation
 */
class HookEntityBaseFieldInfo extends Hook {}
