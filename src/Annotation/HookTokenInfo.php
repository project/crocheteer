<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Token Info.
 *
 * @see \hook_token_info()
 * @see \Drupal\crocheteer\Plugin\Hook\Token\HookTokenInfoPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Token\HookTokenInfoPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookTokenInfoEventSubscriber
 *
 * @Annotation
 */
class HookTokenInfo extends Hook {}
