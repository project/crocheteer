<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Theme.
 *
 * @see \hook_theme()
 * @see \Drupal\crocheteer\Plugin\Hook\Theme\HookThemePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Theme\HookThemePluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookThemeEventSubscriber
 *
 * @Annotation
 */
class HookTheme extends Hook {}
