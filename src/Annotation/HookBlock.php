<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Base class for all Hook Block Annotations.
 *
 * Contains properties global to all Hook Block Annotations. The baseIds
 * property can be used to narrow down objects affected by the Bock Hook,
 * serving as an alternative to BASE_BLOCK_ID-based hook declarations.
 */
abstract class HookBlock extends Hook {

  /**
   * Array of relevant BASE_BLOCK_IDs.
   *
   * @var string[]
   */
  public array $baseBlockIds;

}
