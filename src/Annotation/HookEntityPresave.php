<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Entity Presave.
 *
 * @see \hook_entity_presave()
 * @see \hook_ENTITY_TYPE_presave()
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityPresavePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityPresavePluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookEntityPresaveEventSubscriber
 *
 * @Annotation
 */
class HookEntityPresave extends HookEntity {}
