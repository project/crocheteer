<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Language Switch Links Alter.
 *
 * @see \hook_language_switch_links_alter()
 * @see \Drupal\crocheteer\Plugin\Hook\Language\HookLanguageSwitchLinksAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Language\HookLanguageSwitchLinksAlterPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookLanguageSwitchLinksAlterEventSubscriber
 *
 * @Annotation
 */
class HookLanguageSwitchLinksAlter extends Hook {}
