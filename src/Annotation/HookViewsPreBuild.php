<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Views Pre Build.
 *
 * @see \hook_views_pre_build()
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsPreBuildPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsPreBuildPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookViewsPreBuildEventSubscriber
 *
 * @Annotation
 */
class HookViewsPreBuild extends Hook {}
