<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Base class for all Hook Entity Annotations.
 *
 * Contains properties global to all Hook Entity Annotations. The entityTypes
 * property can be used to narrow down objects affected by the Entity Hook,
 * serving as an alternative to ENTITY_TYPE-based hook declarations.
 */
abstract class HookEntity extends Hook {

  /**
   * Array of relevant ENTITY_TYPEs.
   *
   * @var string[]
   */
  public array $entityTypes;

}
