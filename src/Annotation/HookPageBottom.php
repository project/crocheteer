<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Page Bottom.
 *
 * @see \hook_page_bottom()
 * @see \Drupal\crocheteer\Plugin\Hook\Page\HookPageBottomPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Page\HookPageBottomPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookPageBottomEventSubscriber
 *
 * @Annotation
 */
class HookPageBottom extends Hook {}
