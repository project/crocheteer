<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Themes Installed.
 *
 * @see \hook_themes_installed()
 * @see \Drupal\crocheteer\Plugin\Hook\Theme\HookThemesInstalledPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Theme\HookThemesInstalledPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookThemesInstalledEventSubscriber
 *
 * @Annotation
 */
class HookThemesInstalled extends Hook {}
