<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook Views Query Substitutions.
 *
 * @see \hook_views_query_substitutions()
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsQuerySubstitutionsPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsQuerySubstitutionsPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookViewsQuerySubstitutionsEventSubscriber
 *
 * @Annotation
 */
class HookViewsQuerySubstitutions extends Hook {}
