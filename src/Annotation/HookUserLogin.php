<?php

namespace Drupal\crocheteer\Annotation;

/**
 * Annotation class for Hook User Login.
 *
 * @see \hook_user_login()
 * @see \Drupal\crocheteer\Plugin\Hook\User\HookUserLoginPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\User\HookUserLoginPluginManager
 * @see \Drupal\crocheteer\EventSubscriber\HookUserLoginEventSubscriber
 *
 * @Annotation
 */
class HookUserLogin extends Hook {}
