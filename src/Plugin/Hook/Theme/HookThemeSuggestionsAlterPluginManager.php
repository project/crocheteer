<?php

namespace Drupal\crocheteer\Plugin\Hook\Theme;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\crocheteer\Annotation\HookThemeSuggestionsAlter;
use Drupal\crocheteer\Plugin\Hook\HookPluginManager;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Traversable;

/**
 * Plugin Manager for all Hook Theme Suggestions Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Theme\ThemeSuggestionsAlterEvent $event
 */
class HookThemeSuggestionsAlterPluginManager extends HookPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    parent::__construct(
      $namespaces,
      $cacheBackend,
      $moduleHandler,
      $loggerChannelFactory,
      HookPluginInterface::class,
      HookThemeSuggestionsAlter::class,
      'crocheteer_theme_suggestions_alter'
    );
  }

}
