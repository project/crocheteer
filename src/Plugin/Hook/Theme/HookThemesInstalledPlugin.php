<?php

namespace Drupal\crocheteer\Plugin\Hook\Theme;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Themes Installed Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Theme\ThemesInstalledEvent $event
 */
abstract class HookThemesInstalledPlugin extends HookPlugin {}
