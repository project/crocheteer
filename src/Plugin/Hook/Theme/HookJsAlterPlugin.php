<?php

namespace Drupal\crocheteer\Plugin\Hook\Theme;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Js Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Theme\JsAlterEvent $event
 */
abstract class HookJsAlterPlugin extends HookPlugin {}
