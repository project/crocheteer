<?php

namespace Drupal\crocheteer\Plugin\Hook\Theme;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Library Info Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Theme\LibraryInfoAlterEvent $event
 */
abstract class HookLibraryInfoAlterPlugin extends HookPlugin {}
