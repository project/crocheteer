<?php

namespace Drupal\crocheteer\Plugin\Hook\Theme;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Theme Registry Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Theme\ThemeRegistryAlterEvent $event
 */
abstract class HookThemeRegistryAlterPlugin extends HookPlugin {}
