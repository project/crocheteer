<?php

namespace Drupal\crocheteer\Plugin\Hook\Theme;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Theme Suggestions Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Theme\ThemeSuggestionsAlterEvent $event
 */
abstract class HookThemeSuggestionsAlterPlugin extends HookPlugin {}
