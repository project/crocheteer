<?php

namespace Drupal\crocheteer\Plugin\Hook\Theme;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Template Preprocess Default Variables Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Theme\TemplatePreprocessDefaultVariablesAlterEvent $event
 */
abstract class HookTemplatePreprocessDefaultVariablesAlterPlugin extends HookPlugin {}
