<?php

namespace Drupal\crocheteer\Plugin\Hook\Theme;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Theme Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Theme\ThemeEvent $event
 */
abstract class HookThemePlugin extends HookPlugin {}
