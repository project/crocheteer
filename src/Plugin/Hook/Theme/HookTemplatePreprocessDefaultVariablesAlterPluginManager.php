<?php

namespace Drupal\crocheteer\Plugin\Hook\Theme;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\crocheteer\Annotation\HookTemplatePreprocessDefaultVariablesAlter;
use Drupal\crocheteer\Plugin\Hook\HookPluginManager;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Traversable;

/**
 * Plugin Manager for Hook Template Preprocess Default Variables Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Theme\TemplatePreprocessDefaultVariablesAlterEvent $event
 */
class HookTemplatePreprocessDefaultVariablesAlterPluginManager extends HookPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    parent::__construct(
      $namespaces,
      $cacheBackend,
      $moduleHandler,
      $loggerChannelFactory,
      HookPluginInterface::class,
      HookTemplatePreprocessDefaultVariablesAlter::class,
      'crocheteer_template_preprocess_default_variables_alter'
    );
  }

}
