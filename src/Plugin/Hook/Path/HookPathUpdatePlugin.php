<?php

namespace Drupal\crocheteer\Plugin\Hook\Path;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Path Update Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Path\PathUpdateEvent $event
 */
abstract class HookPathUpdatePlugin extends HookPlugin {}
