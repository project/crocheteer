<?php

namespace Drupal\crocheteer\Plugin\Hook\Path;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\crocheteer\Annotation\HookPathUpdate;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Drupal\crocheteer\Plugin\Hook\HookPluginManager;
use Traversable;

/**
 * Plugin Manager for all Hook Path Update Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Path\PathUpdateEvent $event
 */
class HookPathUpdatePluginManager extends HookPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    parent::__construct(
      $namespaces,
      $cacheBackend,
      $moduleHandler,
      $loggerChannelFactory,
      HookPluginInterface::class,
      HookPathUpdate::class,
      'crocheteer_path_update'
    );
  }

}
