<?php

namespace Drupal\crocheteer\Plugin\Hook\Path;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Path Delete Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Path\PathDeleteEvent $event
 */
abstract class HookPathDeletePlugin extends HookPlugin {}
