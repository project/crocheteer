<?php

namespace Drupal\crocheteer\Plugin\Hook\Path;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Path Insert Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Path\PathInsertEvent $event
 */
abstract class HookPathInsertPlugin extends HookPlugin {}
