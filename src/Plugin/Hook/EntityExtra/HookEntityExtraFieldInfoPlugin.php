<?php

namespace Drupal\crocheteer\Plugin\Hook\EntityExtra;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for Hook Entity Extra Field Info Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\EntityExtra\EntityExtraFieldInfoEvent $event
 */
abstract class HookEntityExtraFieldInfoPlugin extends HookPlugin {}
