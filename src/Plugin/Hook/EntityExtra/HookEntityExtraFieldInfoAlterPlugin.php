<?php

namespace Drupal\crocheteer\Plugin\Hook\EntityExtra;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for Hook Entity Extra Field Info Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\EntityExtra\EntityExtraFieldInfoAlterEvent $event
 */
abstract class HookEntityExtraFieldInfoAlterPlugin extends HookPlugin {}
