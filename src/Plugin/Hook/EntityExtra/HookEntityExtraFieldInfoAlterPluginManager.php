<?php

namespace Drupal\crocheteer\Plugin\Hook\EntityExtra;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\crocheteer\Annotation\HookEntityExtraFieldInfoAlter;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Drupal\crocheteer\Plugin\Hook\HookPluginManager;
use Traversable;

/**
 * Plugin Manager for all Hook Entity Extra Field Info Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\EntityExtra\EntityExtraFieldInfoAlterEvent $event
 */
class HookEntityExtraFieldInfoAlterPluginManager extends HookPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    parent::__construct(
      $namespaces,
      $cacheBackend,
      $moduleHandler,
      $loggerChannelFactory,
      HookPluginInterface::class,
      HookEntityExtraFieldInfoAlter::class,
      'crocheteer_entity_extra_field_info_alter'
    );
  }

}
