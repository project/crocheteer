<?php

namespace Drupal\crocheteer\Plugin\Hook\Form;

use Drupal\crocheteer\Plugin\Hook\HookPluginManager;

/**
 * Base class for all Hook Form Plugin Managers.
 *
 * The formIds property is based on the FORM_ID part from the hook declaration,
 * such as hook_form_FORM_ID_alter().
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Form\BaseFormEvent $event
 */
abstract class HookFormPluginManager extends HookPluginManager {

  /**
   * Allow FORM_IDs restriction.
   *
   * {@inheritdoc}
   */
  protected function getRelevancyProperties() : array {
    return [
      'formIds' => $this->event->getFormId(),
    ];
  }

}
