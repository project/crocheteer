<?php

namespace Drupal\crocheteer\Plugin\Hook\Form;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for Hook Form Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Form\FormAlterEvent $event
 */
abstract class HookFormAlterPlugin extends HookPlugin {}
