<?php

namespace Drupal\crocheteer\Plugin\Hook\Form;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for Hook Field Widget Form Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Form\WidgetFormAlterEvent $event
 */
abstract class HookFieldWidgetFormAlterPlugin extends HookPlugin {}
