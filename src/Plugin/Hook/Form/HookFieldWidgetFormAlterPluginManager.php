<?php

namespace Drupal\crocheteer\Plugin\Hook\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\crocheteer\Annotation\HookFieldWidgetFormAlter;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Drupal\crocheteer\Plugin\Hook\HookPluginManager;
use Traversable;

/**
 * Plugin Manager for all Hook Field Widget Form Alter Plugins.
 *
 * The widgetTypes property is based on the WIDGET_TYPE part from the hook
 * declaration, such as hook_field_widget_WIDGET_TYPE_form_alter().
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Form\WidgetFormAlterEvent $event
 */
class HookFieldWidgetFormAlterPluginManager extends HookPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    parent::__construct(
      $namespaces,
      $cacheBackend,
      $moduleHandler,
      $loggerChannelFactory,
      HookPluginInterface::class,
      HookFieldWidgetFormAlter::class,
      'crocheteer_field_widget_form_alter'
    );
  }

  /**
   * Allow WIDGET_TYPEs restriction.
   *
   * {@inheritdoc}
   */
  protected function getRelevancyProperties() : array {
    /* @var \Drupal\Core\Field\FieldItemList $items */
    $items = $this->event->getContext()['items'];
    $fieldDefinition = $items->getFieldDefinition();
    return [
      'widgetTypes' => $fieldDefinition->getType(),
    ];
  }

}
