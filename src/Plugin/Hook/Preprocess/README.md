If you need preprocessing hooks, make sure you check out the [Preprocess](https://www.drupal.org/project/preprocess) module on drupal.org!
