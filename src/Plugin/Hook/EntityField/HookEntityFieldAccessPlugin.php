<?php

namespace Drupal\crocheteer\Plugin\Hook\EntityField;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for Hook Entity Field Access Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\EntityField\EntityFieldAccessEvent $event
 */
abstract class HookEntityFieldAccessPlugin extends HookPlugin {}
