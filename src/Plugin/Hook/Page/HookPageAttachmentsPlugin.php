<?php

namespace Drupal\crocheteer\Plugin\Hook\Page;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Page Attachments Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Page\PageAttachmentsEvent $event
 */
abstract class HookPageAttachmentsPlugin extends HookPlugin {}
