<?php

namespace Drupal\crocheteer\Plugin\Hook\Page;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Page Bottom Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Page\PageBottomEvent $event
 */
abstract class HookPageBottomPlugin extends HookPlugin {}
