<?php

namespace Drupal\crocheteer\Plugin\Hook\Page;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Page Top Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Page\PageTopEvent $event
 */
abstract class HookPageTopPlugin extends HookPlugin {}
