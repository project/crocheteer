<?php

namespace Drupal\crocheteer\Plugin\Hook\Token;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Token Info Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Token\TokensInfoEvent $event
 */
abstract class HookTokenInfoPlugin extends HookPlugin {}
