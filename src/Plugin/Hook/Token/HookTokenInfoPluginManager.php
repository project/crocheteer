<?php

namespace Drupal\crocheteer\Plugin\Hook\Token;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\crocheteer\Annotation\HookTokenInfo;
use Drupal\crocheteer\Plugin\Hook\HookPluginManager;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Traversable;

/**
 * Plugin Manager for all Hook Token Info Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Token\TokensInfoEvent $event
 */
class HookTokenInfoPluginManager extends HookPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    parent::__construct(
      $namespaces,
      $cacheBackend,
      $moduleHandler,
      $loggerChannelFactory,
      HookPluginInterface::class,
      HookTokenInfo::class,
      'crocheteer_token_info'
    );
  }

}
