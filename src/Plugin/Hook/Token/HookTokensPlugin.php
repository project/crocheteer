<?php

namespace Drupal\crocheteer\Plugin\Hook\Token;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Tokens Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Token\TokensReplacementEvent $event
 */
abstract class HookTokensPlugin extends HookPlugin {}
