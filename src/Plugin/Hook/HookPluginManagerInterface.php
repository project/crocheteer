<?php

namespace Drupal\crocheteer\Plugin\Hook;

use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Interface for all Hook Plugin Managers.
 */
interface HookPluginManagerInterface {

  /**
   * Sets up the Hook Plugin Manager.
   *
   * An Event object containing all Hook parameters/properties must be provided.
   *
   * All Hook Plugin definitions relevant to the current Hook Plugin Manager are
   * retrieved within this method.
   *
   * @param \Drupal\hook_event_dispatcher\Event\EventInterface $event
   *   The Event object containing all Hook parameters/properties.
   */
  public function setup(EventInterface $event) : void;

  /**
   * Execute all Hook Plugins relevant to the current Hook Plugin Manager.
   */
  public function executeHooks() : void;

}
