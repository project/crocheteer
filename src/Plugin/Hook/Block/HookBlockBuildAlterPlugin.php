<?php

namespace Drupal\crocheteer\Plugin\Hook\Block;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for Hook Block Build Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Block\BlockBuildAlterEvent $event
 */
abstract class HookBlockBuildAlterPlugin extends HookPlugin {}
