<?php

namespace Drupal\crocheteer\Plugin\Hook\Block;

use Drupal\crocheteer\Plugin\Hook\HookPluginManager;

/**
 * Base class for all Hook Block Plugin Managers.
 *
 * The baseBlockIds property is based on the BASE_BLOCK_ID part from the hook
 * declaration, such as hook_block_build_BASE_BLOCK_ID_alter().
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Block\BaseBlockEvent $event
 */
abstract class HookBlockPluginManager extends HookPluginManager {

  /**
   * Allow BASE_BLOCK_IDs restriction.
   *
   * {@inheritdoc}
   */
  protected function getRelevancyProperties() : array {
    return [
      'baseBlockIds' => $this->event->getBlock()->getBaseId(),
    ];
  }

}
