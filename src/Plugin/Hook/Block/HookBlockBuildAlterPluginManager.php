<?php

namespace Drupal\crocheteer\Plugin\Hook\Block;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\crocheteer\Annotation\HookBlockBuildAlter;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Traversable;

/**
 * Plugin Manager for all Hook Block Build Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Block\BlockBuildAlterEvent $event
 */
class HookBlockBuildAlterPluginManager extends HookBlockPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    parent::__construct(
      $namespaces,
      $cacheBackend,
      $moduleHandler,
      $loggerChannelFactory,
      HookPluginInterface::class,
      HookBlockBuildAlter::class,
      'crocheteer_block_build_alter'
    );
  }

}
