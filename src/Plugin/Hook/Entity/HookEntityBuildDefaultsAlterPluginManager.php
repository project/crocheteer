<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\crocheteer\Annotation\HookEntityBuildDefaultsAlter;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Traversable;

/**
 * Plugin Manager for all Hook Entity Build Defaults Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityBuildDefaultsAlterEvent $event
 */
class HookEntityBuildDefaultsAlterPluginManager extends HookEntityPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    parent::__construct(
      $namespaces,
      $cacheBackend,
      $moduleHandler,
      $loggerChannelFactory,
      HookPluginInterface::class,
      HookEntityBuildDefaultsAlter::class,
      'crocheteer_entity_build_defaults_alter'
    );
  }

}
