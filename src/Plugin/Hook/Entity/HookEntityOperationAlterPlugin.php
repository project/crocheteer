<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Entity Operation Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityOperationAlterEvent $event
 */
abstract class HookEntityOperationAlterPlugin extends HookPlugin {}
