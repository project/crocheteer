<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity;

use Drupal\crocheteer\Plugin\Hook\HookPluginManager;

/**
 * Base class for all Hook Entity Plugin Managers.
 *
 * The entityTypes property is based on the ENTITY_TYPE part from the hook
 * declaration, such as hook_ENTITY_TYPE_access().
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\BaseEntityEvent $event
 */
abstract class HookEntityPluginManager extends HookPluginManager {

  /**
   * Allow ENTITY_TYPEs restriction.
   *
   * {@inheritdoc}
   */
  protected function getRelevancyProperties() : array {
    return [
      'entityTypes' => $this->event->getEntity()->getEntityTypeId(),
    ];
  }

}
