<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Entity View Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityViewAlterEvent $event
 */
abstract class HookEntityViewAlterPlugin extends HookPlugin {}
