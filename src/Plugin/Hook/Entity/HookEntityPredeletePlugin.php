<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Entity Predelete Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityPredeleteEvent $event
 */
abstract class HookEntityPredeletePlugin extends HookPlugin {}
