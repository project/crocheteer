<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Entity Translation Insert Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityTranslationInsertEvent $event
 */
abstract class HookEntityTranslationInsertPlugin extends HookPlugin {}
