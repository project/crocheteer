<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Entity Update Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityUpdateEvent $event
 */
abstract class HookEntityUpdatePlugin extends HookPlugin {}
