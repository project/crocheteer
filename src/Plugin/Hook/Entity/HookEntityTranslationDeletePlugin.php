<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Entity Translation Delete Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityTranslationDeleteEvent $event
 */
abstract class HookEntityTranslationDeletePlugin extends HookPlugin {}
