<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Entity View Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityViewEvent $event
 */
abstract class HookEntityViewPlugin extends HookPlugin {}
