<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Entity Access Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityAccessEvent $event
 */
abstract class HookEntityAccessPlugin extends HookPlugin {}
