<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\crocheteer\Annotation\HookEntityLoad;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Drupal\crocheteer\Plugin\Hook\HookPluginManager;
use Traversable;

/**
 * Plugin Manager for all Hook Entity Load Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityLoadEvent $event
 */
class HookEntityLoadPluginManager extends HookPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    parent::__construct(
      $namespaces,
      $cacheBackend,
      $moduleHandler,
      $loggerChannelFactory,
      HookPluginInterface::class,
      HookEntityLoad::class,
      'crocheteer_entity_load'
    );
  }

  /**
   * Override method since the Entity Load Event contains an array of entities.
   *
   * {@inheritdoc}
   */
  protected function getRelevancyProperties() : array {
    return [
      'entityTypes' => $this->event->getEntityTypeId(),
    ];
  }

}
