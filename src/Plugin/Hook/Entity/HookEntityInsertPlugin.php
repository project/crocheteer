<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Entity Insert Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityInsertEvent $event
 */
abstract class HookEntityInsertPlugin extends HookPlugin {}
