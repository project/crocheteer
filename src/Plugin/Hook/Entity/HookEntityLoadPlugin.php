<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Entity Load Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityLoadEvent $event
 */
abstract class HookEntityLoadPlugin extends HookPlugin {}
