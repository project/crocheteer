<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Entity Presave Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityPresaveEvent $event
 */
abstract class HookEntityPresavePlugin extends HookPlugin {}
