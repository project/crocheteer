<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Entity Delete Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityDeleteEvent $event
 */
abstract class HookEntityDeletePlugin extends HookPlugin {}
