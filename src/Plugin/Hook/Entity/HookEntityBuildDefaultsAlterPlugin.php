<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Entity Build Defaults Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityBuildDefaultsAlterEvent $event
 */
abstract class HookEntityBuildDefaultsAlterPlugin extends HookPlugin {}
