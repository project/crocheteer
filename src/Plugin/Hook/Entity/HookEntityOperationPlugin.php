<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Entity Operation Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityOperationEvent $event
 */
abstract class HookEntityOperationPlugin extends HookPlugin {}
