<?php

namespace Drupal\crocheteer\Plugin\Hook\User;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook User Cancel Methods Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\User\UserCancelMethodsAlterEvent $event
 */
abstract class HookUserCancelMethodsAlterPlugin extends HookPlugin {}
