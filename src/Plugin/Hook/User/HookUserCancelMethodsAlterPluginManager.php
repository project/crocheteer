<?php

namespace Drupal\crocheteer\Plugin\Hook\User;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\crocheteer\Annotation\HookUserCancelMethodsAlter;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Drupal\crocheteer\Plugin\Hook\HookPluginManager;
use Traversable;

/**
 * Plugin Manager for all Hook User Cancel Methods Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\User\UserCancelMethodsAlterEvent $event
 */
class HookUserCancelMethodsAlterPluginManager extends HookPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    parent::__construct(
      $namespaces,
      $cacheBackend,
      $moduleHandler,
      $loggerChannelFactory,
      HookPluginInterface::class,
      HookUserCancelMethodsAlter::class,
      'crocheteer_user_cancel_methods_alter'
    );
  }

}
