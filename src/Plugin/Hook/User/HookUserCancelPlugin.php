<?php

namespace Drupal\crocheteer\Plugin\Hook\User;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook User Cancel Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\User\UserCancelEvent $event
 */
abstract class HookUserCancelPlugin extends HookPlugin {}
