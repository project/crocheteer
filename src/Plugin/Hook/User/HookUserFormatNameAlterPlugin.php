<?php

namespace Drupal\crocheteer\Plugin\Hook\User;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook User Format Name Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\User\UserFormatNameAlterEvent $event
 */
abstract class HookUserFormatNameAlterPlugin extends HookPlugin {}
