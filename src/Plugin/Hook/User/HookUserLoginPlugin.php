<?php

namespace Drupal\crocheteer\Plugin\Hook\User;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook User Login Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\User\UserLoginEvent $event
 */
abstract class HookUserLoginPlugin extends HookPlugin {}
