<?php

namespace Drupal\crocheteer\Plugin\Hook\User;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook User Logout Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\User\UserLogoutEvent $event
 */
abstract class HookUserLogoutPlugin extends HookPlugin {}
