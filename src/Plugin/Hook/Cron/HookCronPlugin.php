<?php

namespace Drupal\crocheteer\Plugin\Hook\Cron;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for Hook Cron Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Cron\CronEvent $event
 */
abstract class HookCronPlugin extends HookPlugin {}
