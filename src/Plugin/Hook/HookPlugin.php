<?php

namespace Drupal\crocheteer\Plugin\Hook;

use Drupal\Component\Plugin\PluginBase;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Base class for all Hook Plugins.
 */
abstract class HookPlugin extends PluginBase implements HookPluginInterface {

  /**
   * The Event object containing all Hook parameters.
   *
   * @var \Drupal\hook_event_dispatcher\Event\EventInterface
   */
  protected EventInterface $event;

  /**
   * {@inheritdoc}
   */
  public function setup(EventInterface $event) : void {
    $this->event = $event;
  }

}
