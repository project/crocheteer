<?php

namespace Drupal\crocheteer\Plugin\Hook\Views;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Views Post Render Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Views\ViewsPostRenderEvent $event
 */
abstract class HookViewsPostRenderPlugin extends HookPlugin {}
