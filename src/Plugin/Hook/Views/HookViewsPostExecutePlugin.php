<?php

namespace Drupal\crocheteer\Plugin\Hook\Views;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Views Post Execute Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Views\ViewsPostExecuteEvent $event
 */
abstract class HookViewsPostExecutePlugin extends HookPlugin {}
