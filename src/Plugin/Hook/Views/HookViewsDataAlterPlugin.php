<?php

namespace Drupal\crocheteer\Plugin\Hook\Views;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Views Data Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Views\ViewsDataAlterEvent $event
 */
abstract class HookViewsDataAlterPlugin extends HookPlugin {}
