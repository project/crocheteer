<?php

namespace Drupal\crocheteer\Plugin\Hook\Views;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Views Post Build Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Views\ViewsPostBuildEvent $event
 */
abstract class HookViewsPostBuildPlugin extends HookPlugin {}
