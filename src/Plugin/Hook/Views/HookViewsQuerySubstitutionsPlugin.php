<?php

namespace Drupal\crocheteer\Plugin\Hook\Views;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Views Query Substitutions Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Views\ViewsQuerySubstitutionsEvent $event
 */
abstract class HookViewsQuerySubstitutionsPlugin extends HookPlugin {}
