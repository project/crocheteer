<?php

namespace Drupal\crocheteer\Plugin\Hook\Views;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Views Pre Execute Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Views\ViewsPreExecuteEvent $event
 */
abstract class HookViewsPreExecutePlugin extends HookPlugin {}
