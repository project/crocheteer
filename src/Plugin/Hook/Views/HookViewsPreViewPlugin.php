<?php

namespace Drupal\crocheteer\Plugin\Hook\Views;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Views Pre View Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Views\ViewsPreViewEvent $event
 */
abstract class HookViewsPreViewPlugin extends HookPlugin {}
