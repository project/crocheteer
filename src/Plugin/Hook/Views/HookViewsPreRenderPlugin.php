<?php

namespace Drupal\crocheteer\Plugin\Hook\Views;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Views Pre Render Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Views\ViewsPreRenderEvent $event
 */
abstract class HookViewsPreRenderPlugin extends HookPlugin {}
