<?php

namespace Drupal\crocheteer\Plugin\Hook\Views;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Views Query Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Views\ViewsQueryAlterEvent $event
 */
abstract class HookViewsQueryAlterPlugin extends HookPlugin {}
