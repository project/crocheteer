<?php

namespace Drupal\crocheteer\Plugin\Hook\Views;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Views Pre Build Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Views\ViewsPreBuildEvent $event
 */
abstract class HookViewsPreBuildPlugin extends HookPlugin {}
