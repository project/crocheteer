<?php

namespace Drupal\crocheteer\Plugin\Hook\Views;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Views Data Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Views\ViewsDataEvent $event
 */
abstract class HookViewsDataPlugin extends HookPlugin {}
