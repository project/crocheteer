<?php

namespace Drupal\crocheteer\Plugin\Hook\EntityType;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for Hook Entity Type Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\EntityType\EntityTypeAlterEvent $event
 */
abstract class HookEntityTypeAlterPlugin extends HookPlugin {}
