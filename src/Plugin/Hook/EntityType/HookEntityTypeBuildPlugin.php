<?php

namespace Drupal\crocheteer\Plugin\Hook\EntityType;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for Hook Entity Type Build Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\EntityType\EntityTypeBuildEvent $event
 */
abstract class HookEntityTypeBuildPlugin extends HookPlugin {}
