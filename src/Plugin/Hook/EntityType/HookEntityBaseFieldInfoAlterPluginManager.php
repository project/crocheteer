<?php

namespace Drupal\crocheteer\Plugin\Hook\EntityType;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\crocheteer\Annotation\HookEntityBaseFieldInfoAlter;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Drupal\crocheteer\Plugin\Hook\HookPluginManager;
use Traversable;

/**
 * Plugin Manager for all Hook Entity Base Field Info Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\EntityType\EntityBaseFieldInfoAlterEvent $event
 */
class HookEntityBaseFieldInfoAlterPluginManager extends HookPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    parent::__construct(
      $namespaces,
      $cacheBackend,
      $moduleHandler,
      $loggerChannelFactory,
      HookPluginInterface::class,
      HookEntityBaseFieldInfoAlter::class,
      'crocheteer_entity_base_field_info'
    );
  }

}
