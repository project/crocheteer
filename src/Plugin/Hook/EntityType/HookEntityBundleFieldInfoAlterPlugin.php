<?php

namespace Drupal\crocheteer\Plugin\Hook\EntityType;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for Hook Entity Bundle Field Info Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\EntityType\EntityBundleFieldInfoAlterEvent $event
 */
abstract class HookEntityBundleFieldInfoAlterPlugin extends HookPlugin {}
