<?php

namespace Drupal\crocheteer\Plugin\Hook\EntityType;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for Hook Entity Base Field Info Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\EntityType\EntityBaseFieldInfoEvent $event
 */
abstract class HookEntityBaseFieldInfoPlugin extends HookPlugin {}
