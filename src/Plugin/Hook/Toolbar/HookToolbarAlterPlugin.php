<?php

namespace Drupal\crocheteer\Plugin\Hook\Toolbar;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for all Hook Toolbar Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Toolbar\ToolbarAlterEvent $event
 */
abstract class HookToolbarAlterPlugin extends HookPlugin {}
