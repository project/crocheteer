<?php

namespace Drupal\crocheteer\Plugin\Hook;

use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Interface for all Hook Plugins.
 *
 * This is the base interface used by Plugin Managers when they're being
 * constructed and specifying their pluginInterface parameter.
 */
interface HookPluginInterface {

  /**
   * Sets up the Hook Plugin properties.
   *
   * An Event object containing all Hook parameters/properties must be provided.
   * All necessary Hook properties are mapped directly within the Hook Plugin,
   * from the Event object, for quicker access. Said properties are always
   * accessible via the Hook Plugin's event property.
   *
   * Note that some Event methods may also be mapped to the Hook Plugin because
   * of the way they must manipulate the Hook Plugin properties.
   *
   * @param \Drupal\hook_event_dispatcher\Event\EventInterface $event
   *   The Event object containing all Hook parameters/properties.
   */
  public function setup(EventInterface $event) : void;

  /**
   * Execute all Hook Plugin operations.
   *
   * This should call other protected or private methods contained in the class.
   * Ideally you want as little conditional statements as possible in here.
   */
  public function hook() : void;

}
