<?php

namespace Drupal\crocheteer\Plugin\Hook\Language;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Base class for Hook Language Switch Links Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Language\LanguageSwitchLinksAlterEvent $event
 */
abstract class HookLanguageSwitchLinksAlterPlugin extends HookPlugin {}
