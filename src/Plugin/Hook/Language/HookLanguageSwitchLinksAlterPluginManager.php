<?php

namespace Drupal\crocheteer\Plugin\Hook\Language;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\crocheteer\Annotation\HookLanguageSwitchLinksAlter;
use Drupal\crocheteer\Plugin\Hook\HookPluginManager;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Traversable;

/**
 * Plugin Manager for all Hook Language Switch Links Alter Plugins.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Language\LanguageSwitchLinksAlterEvent $event
 */
class HookLanguageSwitchLinksAlterPluginManager extends HookPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    parent::__construct(
      $namespaces,
      $cacheBackend,
      $moduleHandler,
      $loggerChannelFactory,
      HookPluginInterface::class,
      HookLanguageSwitchLinksAlter::class,
      'crocheteer_language_switch_links_alter'
    );
  }

}
