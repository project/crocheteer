# Crocheteer - Example

- [Introduction](#introduction)
- [Purpose](#purpose)
- [Structure](#structure)
- [How To Use](#how-to-use)
- [Examples](#examples)
- [Requirements](#requirements)

## Introduction

Example module for the Crocheteer module.

## Purpose

This module is meant to act as a reference when implementing Hook Annotations in custom project-specific modules.

## Structure

All example Annotations are contained in `src/Plugin/crocheteer/Hook`, so make sure to check that out.

## How To Use

Three simple steps:

- Copy the desired Hook class from this module's `src/Plugin/crocheteer/Hook` directory
- Paste the copied Hook class in the desired module's `src/Plugin/crocheteer/Hook` directory
- Adjust as needed (file name & code)

Please note that all example classes make use of dependency injection mechanisms. Feel free to remove unneeded class implementations, properties and methods from your code if your Hook has no need for any kind of dependency injection.

## Examples

See this module's `src/Plugin/crocheteer/Hook` directory.

## Requirements

PHP minimal version required: `7.4`
