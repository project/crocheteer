<?php

namespace Drupal\crocheteer_example\Plugin\crocheteer\Hook;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\hook_event_dispatcher\Value\Token;
use Drupal\hook_event_dispatcher\Value\TokenType;
use Drupal\crocheteer\Plugin\Hook\Token\HookTokenInfoPlugin;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Example Class for Hook Token Info.
 *
 * @HookTokenInfo(
 *   id = "crocheteer_example_token_info",
 *   title = @Translation("Crocheteer Example: Token Info"),
 * )
 */
final class HookTokenInfoExample extends HookTokenInfoPlugin implements ContainerFactoryPluginInterface {

  /**
   * The injected Drupal Messenger dependency.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  private MessengerInterface $messenger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) : HookPluginInterface {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('messenger')
    );
  }

  /**
   * HookTokenInfoExample constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $pluginId
   *   The plugin ID for the plugin instance.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Drupal Messenger.
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, MessengerInterface $messenger) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   *
   * If you have the Token module enabled, you can see this Token being
   * registered on the module's help page.
   *
   * @see /admin/help/token
   */
  public function hook() : void {
    $tokenType = TokenType::create('crocheteer_example', t('Crocheteer Example'))->setDescription('Tokens related to the Crocheteer Example module.');
    $this->event->addTokenType($tokenType);
    $token = Token::create('crocheteer_example', 'hello', t('Hello'))->setDescription('Hello string dummy text.');
    $this->event->addToken($token);
    $this->messenger->addStatus('Crocheteer Example: Token Info executed! The ' . $tokenType->getName() . ' ' . $token->getName() . ' token was registered!');
  }

}
