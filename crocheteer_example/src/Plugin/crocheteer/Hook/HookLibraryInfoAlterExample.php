<?php

namespace Drupal\crocheteer_example\Plugin\crocheteer\Hook;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\crocheteer\Plugin\Hook\Theme\HookLibraryInfoAlterPlugin;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Example Class for Hook Library Info Alter.
 *
 * @HookLibraryInfoAlter(
 *   id = "crocheteer_example_library_info_alter",
 *   title = @Translation("Crocheteer Example: Library Info Alter"),
 * )
 */
final class HookLibraryInfoAlterExample extends HookLibraryInfoAlterPlugin implements ContainerFactoryPluginInterface {

  /**
   * The injected Drupal Messenger dependency.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  private MessengerInterface $messenger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) : HookPluginInterface {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('messenger')
    );
  }

  /**
   * HookLibraryInfoAlterExample constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $pluginId
   *   The plugin ID for the plugin instance.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Drupal Messenger.
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, MessengerInterface $messenger) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function hook() : void {
    $library = 'jquery.farbtastic';
    if ($this->event->getExtension() === 'core' && in_array($library, $this->event->getLibraries(), TRUE)) {
      $this->messenger->addStatus('Crocheteer Example: Library Info Alter executed for the ' . $library . ' library from the ' . $this->event->getExtension() . ' extension!');
    }
  }

}
