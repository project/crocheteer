<?php

namespace Drupal\crocheteer_example\Plugin\crocheteer\Hook;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\crocheteer\Plugin\Hook\Form\HookFieldWidgetFormAlterPlugin;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Example class for Hook Field Widget Form Alter.
 *
 * @HookFieldWidgetFormAlter(
 *   id = "crocheteer_example_field_widget_form_alter",
 *   title = @Translation("Crocheteer Example: Field Widget Form Alter"),
 *   widgetTypes = {
 *     "text_textfield",
 *   },
 * )
 */
final class HookFieldWidgetFormAlterExample extends HookFieldWidgetFormAlterPlugin implements ContainerFactoryPluginInterface {

  /**
   * The injected Drupal Messenger dependency.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  private MessengerInterface $messenger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) : HookPluginInterface {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('messenger')
    );
  }

  /**
   * HookFieldWidgetFormAlterExample constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $pluginId
   *   The plugin ID for the plugin instance.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Drupal Messenger.
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, MessengerInterface $messenger) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function hook() : void {
    /* @var \Drupal\Core\Field\FieldItemList $items */
    $items = $this->event->getContext()['items'];
    if ($items->getName() === 'title') {
      $this->messenger->addStatus('Crocheteer Example: Field Widget Form Alter executed for the ' . $items->getName() . ' field widget form!');
    }
  }

}
