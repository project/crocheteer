<?php

namespace Drupal\crocheteer_example\Plugin\crocheteer\Hook;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\crocheteer\Plugin\Hook\EntityType\HookEntityTypeAlterPlugin;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Example class for Hook Entity Type Alter.
 *
 * @HookEntityTypeAlter(
 *   id = "crocheteer_example_entity_type_alter",
 *   title = @Translation("Crocheteer Example: Entity Type Alter"),
 * )
 */
final class HookEntityTypeAlterExample extends HookEntityTypeAlterPlugin implements ContainerFactoryPluginInterface {

  /**
   * The injected Drupal Logger Channel dependency.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private LoggerChannelInterface $loggerChannel;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) : HookPluginInterface {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('logger.factory')
    );
  }

  /**
   * HookEntityTypeAlterExample constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $pluginId
   *   The plugin ID for the plugin instance.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The Drupal Logger Channel Factory.
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, LoggerChannelFactoryInterface $loggerChannelFactory) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->loggerChannel = $loggerChannelFactory->get($this->pluginDefinition['id']);
  }

  /**
   * {@inheritdoc}
   */
  public function hook() : void {
    $entityTypes = [];
    foreach ($this->event->getEntityTypes() as $entityType) {
      $entityTypes[] = $entityType->id();
    }
    $this->loggerChannel->info('Crocheteer Example: Entity Type Alter executed for the ' . implode(', ', $entityTypes) . ' entity types!');
  }

}
