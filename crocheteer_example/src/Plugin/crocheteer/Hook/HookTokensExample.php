<?php

namespace Drupal\crocheteer_example\Plugin\crocheteer\Hook;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Drupal\crocheteer\Plugin\Hook\Token\HookTokensPlugin;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Example Class for Hook Tokens.
 *
 * @HookTokens(
 *   id = "crocheteer_example_tokens",
 *   title = @Translation("Crocheteer Example: Tokens"),
 * )
 */
final class HookTokensExample extends HookTokensPlugin implements ContainerFactoryPluginInterface {

  /**
   * The injected Drupal Messenger dependency.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  private MessengerInterface $messenger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) : HookPluginInterface {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('messenger')
    );
  }

  /**
   * HookTokensExample constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $pluginId
   *   The plugin ID for the plugin instance.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Drupal Messenger.
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, MessengerInterface $messenger) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   *
   * If you have the Pathauto module enabled, you can use this Token in the
   * module's patterns configuration page.
   *
   * @see /admin/config/search/path/patterns
   */
  public function hook() : void {
    if ($this->event->forToken('crocheteer_example', 'hello')) {
      $this->event->setReplacementValue('crocheteer_example', 'hello', 'hello');
      $this->messenger->addStatus('Crocheteer Example: Tokens executed!');
    }
  }

}
