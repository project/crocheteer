<?php

namespace Drupal\crocheteer_example\Plugin\crocheteer\Hook;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Drupal\crocheteer\Plugin\Hook\Theme\HookTemplatePreprocessDefaultVariablesAlterPlugin;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Example Class for Hook Template Preprocess Default Variables Alter.
 *
 * @HookTemplatePreprocessDefaultVariablesAlter(
 *   id = "crocheteer_example_template_preprocess_default_variables_alter",
 *   title = @Translation("Crocheteer Example: Template Preprocess Default Variables Alter"),
 * )
 */
final class HookTemplatePreprocessDefaultVariablesAlterExample extends HookTemplatePreprocessDefaultVariablesAlterPlugin implements ContainerFactoryPluginInterface {

  /**
   * The injected Drupal Messenger dependency.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  private MessengerInterface $messenger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) : HookPluginInterface {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('messenger')
    );
  }

  /**
   * HookTemplatePreprocessDefaultVariablesAlterExample constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $pluginId
   *   The plugin ID for the plugin instance.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Drupal Messenger.
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, MessengerInterface $messenger) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function hook() : void {
    $this->messenger->addStatus('Crocheteer Example: Template Preprocess Default Variables Alter executed!');
  }

}
